# Otázky na prezentaci
**Zodpovězte prosím pro třídu pravdivě následující otázky**
| otázka                                        | vyjádření                       |
| :-------------------------------------------- | :------------------------------ |
| jak dlouho mi tvorba zabrala - **čistý čas**  | 4  hodin                        |
| odkud jsem čerpal inspiraci                   | od ostatních žáků                           |
| odkaz na video                                | https://youtu.be/0w0FBPPn7OM?si=4KNWZpj93_xCJPxt                     |
| jak se mi to podařilo rozplánovat             | všechno za 1 den 					      |
| proč jsem zvolil tento design                 | sebral jsem to, co bylo po ruce |
| zapojení                                      | ![My Image](/dokumentace/schema/foto_schema_zapojeni.jpg)         |
| z jakých součástí se zapojení skládá          | LED páska, měřič teploty a vlhkosti, WEMOS, rezistor   |
| realizace                                     | ![My Image2](/dokumentace/fotky/IMG_0192.jpg) |
| UI                                            | ![My Image3](/dokumentace/fotky/UI.png)               |
| co se mi povedlo                              |             |
| co se mi nepovedlo/příště bych udělal/a jinak | dokopat se do toho, abych to udělal dříve            |
| zhodnocení celé tvorby (návrh známky)         | 3                               |